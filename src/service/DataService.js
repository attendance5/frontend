import http from "../http-common";

class DataService {
    getAll() {
        return http.get("/absen")
    }
    searchByName(name) {
        return http.get(`/absen?name=${name}`)
    }
    searchByDate(date) {
        return http.get(`/absen?date=${date}`)
    }
    addAbsen(data) {
        return http.post("/absen", data)
    }
}

export default new DataService;