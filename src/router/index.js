import { createWebHistory, createRouter } from "vue-router";
import Home from "../components/HomeAbsen.vue";
import ListAbsen from "../components/ListAbsen.vue"
import FormAbsen from "../components/FormAbsen.vue"
import Navbar from "../components/Navbar.vue"
import Sidebar from "../components/Sidebar.vue"

const routes = [
    {
        path: "/",
        name: "home",
        component: Home,
    },
    {
        path: "/absen",
        name: "absen",
        component: ListAbsen,
    },
    {
        path: "/form",
        name: "form",
        component: FormAbsen,
    },
    {
        path: "/navbar",
        name: "navbar",
        component: Navbar,
    },
    {
        path: "/Sidebar",
        name: "Sidebar",
        component: Sidebar,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});
export default router;